﻿using ApiPotencialPayment.Controllers;
using ApiPotencialPayment.Service;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using ApiPotencialPayment.Domain.Models;
using System.Collections.Generic;
using static ApiPotencialPayment.Domain.Models.Enum;
using ApiPotencialPayment.Domain.Models.Saida;

namespace ApiPotencialPaymentTest
{
    public class VendaControllerTest
    {
        private const string vendaListCacheKey = "vendasList";
        private readonly Vendas _vendas;
        VendaController _controller;
        IVendaServices _service;

        public VendaControllerTest()
        {
            var cache = new MemoryCache(new MemoryCacheOptions());
            _vendas = new Vendas()
            {
                new Venda() { Id = 1, DataVenda = DateTime.Now.AddDays(-1), StatusVenda = StatusVenda.Cancelada,
                    Vendedor = new Vendedor { CPF = "123456789", Email = "email@mail", Nome = "Vendedor 1", Telefone = "912345678"},
                    Itens = new List<Item>() { new Item { Descricao = "Shampoo", Identificador = "1", Valor = 10.00F  } }
                },

                new Venda() { Id = 2, DataVenda = DateTime.Now, StatusVenda = StatusVenda.AguardandoPagamento,
                    Vendedor = new Vendedor { CPF = "31744491038", Email = "email@mail", Nome = "Vendedor 2", Telefone = "938838857"},
                    Itens = new List<Item>() { new Item { Descricao = "pião", Identificador = "2", Valor = 20.00F  } }
                },

                new Venda() { Id = 3, DataVenda = DateTime.Now.AddDays(-2), StatusVenda = StatusVenda.PagamentoAprovado,
                    Vendedor = new Vendedor { CPF = "24910888004", Email = "email@mail", Nome = "Vendedor 3", Telefone = "935267131"},
                    Itens = new List<Item>() { new Item { Descricao = "boneco", Identificador = "3", Valor = 30.00F  } }
                },

                new Venda() { Id = 4, DataVenda = DateTime.Now.AddDays(-5), StatusVenda = StatusVenda.Entregue,
                    Vendedor = new Vendedor { CPF = "43420765002", Email = "email@mail", Nome = "Vendedor 4", Telefone = "920058884"},
                    Itens = new List<Item>() { new Item { Descricao = "carro", Identificador = "4", Valor = 40.00F  } }
                },

                new Venda() { Id = 5, DataVenda = DateTime.Now.AddDays(-3), StatusVenda = StatusVenda.EnviadoTranportadora,
                    Vendedor = new Vendedor { CPF = "30841782083", Email = "email@mail", Nome = "Vendedor 5", Telefone = "925840756"},
                    Itens = new List<Item>() { new Item { Descricao = "balão", Identificador = "5", Valor = 50.00F  } }
                }
            };

            _service = new VendaServices();

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(50)); //Define tempo de expiração da memória cache.

            cache.Set(vendaListCacheKey, _vendas, cacheEntryOptions);

            _controller = new VendaController(cache, _service);
        }

        [Fact]
        public void BuscarVenda_QuandoProcurado1_ReturnNotNullAndOkObjectResult()
        {
            //Act
            var result = _controller.BuscarVenda(1);

            //Assert
            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void BuscarVenda_QuandoProcurado12_ReturnNotFoundResult()
        {
            //Act
            var result = _controller.BuscarVenda(12);

            //Assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void RegistrarVenda_NaoInformarItens_ReturnBadRequest()
        {
            //Arrange
            var input = new InRegistrarVenda
            {
               Vendedor = new InVendedor
               {
                   CPF = "12345678901",
                   Email = "email@email.com",
                   Nome = "TesteRegistro",
                   Telefone = "987655678"
               },
               Itens = new List<InItem>()
            };

            //Act
            var result = _controller.RegistrarVenda(input);

            //Assert
            Assert.True(input.Itens.Count == 0);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void RegistrarVenda_NaoInformarCPFVendedor_ReturnBadRequest()
        {
            //Arrange
            var input = new InRegistrarVenda
            {
                Vendedor = new InVendedor
                {
                    Email = "email@email.com",
                    Nome = "TesteRegistro",
                    Telefone = "987655678"
                },
                Itens = new List<InItem>()
                {
                    new InItem() {
                        Descricao = "ItemTeste",
                        Identificador = "10",
                        Valor = 98.00F
                    }
                }
            };

            //Act
            var result = _controller.RegistrarVenda(input);

            //Assert
            Assert.True(string.IsNullOrEmpty(input.Vendedor.CPF));
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public void RegistrarVenda_InformarVendedorEAoMenosUmItem_ReturnVenda()
        {
            //Arrange
            var input = new InRegistrarVenda
            {
                Vendedor = new InVendedor
                {
                    CPF = "12345678901",
                    Email = "email@email.com",
                    Nome = "TesteRegistro",
                    Telefone = "987655678"
                },
                Itens = new List<InItem>()
                {
                    new InItem() {
                        Descricao = "ItemTeste",
                        Identificador = "10",
                        Valor = 98.00F
                    }
                }
            };

            //Act
            var result = _controller.RegistrarVenda(input);

            //Assert
            Assert.True(input.Itens.Count >= 1);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void RegistrarVenda_InformarDadosDaVenda_ReturnVendaComStatusAgurdandoPagamento()
        {
            //Arrange
            var input = new InRegistrarVenda
            {
                Vendedor = new InVendedor
                {
                    CPF = "12345678901",
                    Email = "email@email.com",
                    Nome = "TesteRegistro",
                    Telefone = "987655678"
                },
                Itens = new List<InItem>()
                {
                    new InItem() {
                        Descricao = "ItemTeste",
                        Identificador = "10",
                        Valor = 98.00F
                    }
                }
            };

            //Act
            var result = _controller.RegistrarVenda(input) as OkObjectResult;
            var venda = result.Value as Venda;

            //result.Value
            Assert.True(venda.StatusVenda == StatusVenda.AguardandoPagamento);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void AtualizarVenda_InformarIdVendaEStatusCancelada_ReturnOK()
        {
            //Act
            var result = _controller.AtualizarVenda(2, StatusVenda.Cancelada) as OkObjectResult;
            var ret = result.Value as OutRetorno;

            //result.Value
            Assert.True(ret.Retorno == "Status Atualizado com Sucesso!");
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void AtualizarVenda_InformarIdVendaEStatusCancelada_ReturnBadRequest()
        {
            //Act
            var result = _controller.AtualizarVenda(5, StatusVenda.Cancelada) as BadRequestObjectResult;
            var ret = result.Value as OutRetorno;

            //result.Value
            Assert.False(ret.Retorno == "Status Atualizado com Sucesso!");
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
