﻿using System;
using System.Collections.Generic;
using static ApiPotencialPayment.Domain.Models.Enum;

namespace ApiPotencialPayment.Domain.Models
{
    public class Vendas : List<Venda> { }

    public class Venda
    {
        public int Id { get; set; } 

        public Vendedor Vendedor { get; set; }
        
        public List<Item> Itens { get; set; }

        public DateTime DataVenda { get; set; }

        public StatusVenda StatusVenda { get; set; }
    }

    /// <summary>
    /// Classe Vendedor
    /// </summary>
    public class Vendedor
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// CPF
        /// </summary>
        public string CPF { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Nome { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Telefone
        /// </summary>
        public string Telefone { get; set; }
    }

    /// <summary>
    /// Classe Item
    /// </summary>
    public class Item
    {
        /// <summary>
        /// Identificador do Item
        /// </summary>
        public string Identificador { get; set; }

        /// <summary>
        /// Descrição do Item
        /// </summary>
        public string Descricao { get; set; }

        /// <summary>
        /// Valor do Item
        /// </summary>
        public float Valor { get; set; }
    }
}
