﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApiPotencialPayment.Domain.Models
{
    /// <summary>
    /// Classe InRegistrarVenda
    /// </summary>
    public class InRegistrarVenda
    {
        /// <summary>
        /// Dados do Vendedor que efetivou a venda
        /// </summary>
        [Required]
        public InVendedor Vendedor { get; set; }

        /// <summary>
        /// Itens que foram vendidos.
        /// </summary>
        [Required]
        public List<InItem> Itens { get; set; }
    }

    /// <summary>
    /// Classe Vendedor
    /// </summary>
    public class InVendedor
    {
        /// <summary>
        /// CPF
        /// </summary>
        [Required, StringLength(14)]
        public string CPF { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [Required]
        public string Nome { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Telefone
        /// </summary>
        public string Telefone { get; set; }
    }

    /// <summary>
    /// Classe Item
    /// </summary>
    public class InItem
    {
        /// <summary>
        /// Identificador do Item
        /// </summary>
        [Required, StringLength(15)]
        public string Identificador { get; set; }

        /// <summary>
        /// Descrição do Item
        /// </summary>
        [Required]
        public string Descricao { get; set; }

        /// <summary>
        /// Valor do Item
        /// </summary>
        [Required]
        public float Valor { get; set; }
    }
}
