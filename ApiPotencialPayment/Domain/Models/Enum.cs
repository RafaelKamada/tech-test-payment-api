﻿using System.ComponentModel;

namespace ApiPotencialPayment.Domain.Models
{
    public class Enum
    {
        public enum StatusVenda
        {
            /// <summary>
            /// Status Inicial, é somente gerado quando for registrado uma venda
            /// </summary>
            [DefaultValue("0"), Description("Aguardando Pagamento")]
            AguardandoPagamento,

            [DefaultValue("1"), Description("Pagamento Aprovado")]
            PagamentoAprovado,

            [DefaultValue("2"), Description("Enviado para Tranportadora")]
            EnviadoTranportadora,

            [DefaultValue("3"), Description("Entregue")]
            Entregue,

            [DefaultValue("4"), Description("Cancelada")]
            Cancelada
        }
    }
}
