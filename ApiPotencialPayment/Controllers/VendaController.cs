﻿using ApiPotencialPayment.Domain.Models;
using ApiPotencialPayment.Domain.Models.Saida;
using ApiPotencialPayment.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using static ApiPotencialPayment.Domain.Models.Enum;

namespace ApiPotencialPayment.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : Controller
    {
        private const string vendaListCacheKey = "vendasList";
        private readonly IMemoryCache _memoryCache;
        private readonly IVendaServices _vendaServices;

        public VendaController(IMemoryCache memoryCache, IVendaServices vendaServices)
        {
            _memoryCache = memoryCache;
            _vendaServices = vendaServices;
        }

        /// <summary>
        /// Recebe os dados do vendedor + itens vendidos. Registra venda com status "Aguardando pagamento".
        /// </summary>
        /// <returns></returns>
        /// <param name="pEntrada">Objeto InRegistrarVenda</param>
        [HttpPost]
        public IActionResult RegistrarVenda(InRegistrarVenda pEntrada)
        {
            Vendas vendasAux = new Vendas();

            if (_memoryCache.TryGetValue(vendaListCacheKey, out Vendas cacheValue))
            {
                vendasAux = cacheValue;
            }

            if (!_vendaServices.CriticarRegistro(pEntrada))
            {
                return BadRequest(pEntrada);
            }

            vendasAux = _vendaServices.RegistrarVenda(pEntrada, vendasAux);

            var cacheEntryOptions = new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(50)); //Define tempo de expiração da memória cache.

            _memoryCache.Set(vendaListCacheKey, vendasAux, cacheEntryOptions);

            return Ok(vendasAux.Last());
        }

        /// <summary>
        /// Busca pelo ID da venda.
        /// </summary>
        /// <returns></returns>
        /// <param name="idVenda">ID da Venda</param>
        [HttpGet]
        public IActionResult BuscarVenda([FromQuery] int idVenda)
        {
            Vendas vendas = new Vendas();

            if (_memoryCache.TryGetValue(vendaListCacheKey, out Vendas cacheValue))
            {
                vendas = cacheValue;
            }

            Venda venda = _vendaServices.BuscarVenda(idVenda, vendas);

            if (venda != null)
            {
                return Ok(venda);
            }

            return NotFound();            
        }

         
        /// <summary>
        /// Permite que seja atualizado o status da venda.
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public IActionResult AtualizarVenda([FromQuery] int idVenda, [FromQuery] StatusVenda pStatus)
        {
            Vendas vendas = new Vendas();

            if (_memoryCache.TryGetValue(vendaListCacheKey, out Vendas cacheValue))
            {
                vendas = cacheValue;
            }

            OutRetorno ret = new OutRetorno();

            if (_vendaServices.AtualizarVenda(idVenda, pStatus, vendas))
            {
                ret.Retorno = "Status Atualizado com Sucesso!";
                return Ok(ret);
            }
            else
            {
                Venda venda = _vendaServices.BuscarVenda(idVenda, vendas);
                ret.Retorno = string.Format("Não permitido alteração do Status: {0} para: {1}", venda.StatusVenda, pStatus);
                return BadRequest(ret);
            }  
        }
    }
}
