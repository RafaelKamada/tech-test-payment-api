﻿using ApiPotencialPayment.Domain.Models;
using ApiPotencialPayment.Domain.Models.Saida;
using static ApiPotencialPayment.Domain.Models.Enum;

namespace ApiPotencialPayment.Service
{
    public interface IVendaServices
    {
        public Vendas RegistrarVenda(InRegistrarVenda pEntrada, Vendas pVendas);
        
        public Venda BuscarVenda(int idVenda, Vendas pVendas);

        public bool AtualizarVenda(int idVenda, StatusVenda pStatus, Vendas pVendas);

        public bool CriticarRegistro(InRegistrarVenda pEntrada);
    }
}
