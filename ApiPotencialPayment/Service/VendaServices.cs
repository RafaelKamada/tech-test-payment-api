﻿using ApiPotencialPayment.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using static ApiPotencialPayment.Domain.Models.Enum;

namespace ApiPotencialPayment.Service
{
    public class VendaServices: IVendaServices
    {
        public bool AtualizarVenda(int idVenda, StatusVenda pStatus, Vendas pVendas)
        {
            bool statusValido = false;

            if (pVendas.Exists(e => e.Id == idVenda))
            {
                Venda vendaAux = pVendas.FirstOrDefault(f => f.Id == idVenda);
                statusValido = ValidaTrocaStatus(pStatus, vendaAux);

                if (statusValido)
                {
                    vendaAux.StatusVenda = pStatus;
                }

                return statusValido;
            }

            return statusValido;
        }

        public Venda BuscarVenda(int idVenda, Vendas pVendas)
        {
            if (pVendas.Exists(e => e.Id == idVenda))
            {
                return pVendas.Where(w => w.Id == idVenda).FirstOrDefault();
            }
            else
                return null;
        }

        /// <summary>
        /// Convert modelo de entrada
        /// </summary>
        /// <param name="pEntrada"></param>
        /// <param name="pVendaExistentes"></param>
        /// <returns></returns>
        public Vendas ConverterModel(InRegistrarVenda pEntrada, Vendas pVendaExistentes)
        {
            Venda vendaAux = new Venda();
            Vendedor vendedorAux = new Vendedor();
            List<Item> itensAux = new List<Item>();
            Vendas vendasAux = pVendaExistentes;

            if (vendasAux.Any())
            {
                vendaAux.Id = vendasAux.LastOrDefault().Id + 1;
                vendedorAux.Id = vendasAux.LastOrDefault().Vendedor.Id + 1;
            }
            else
            {
                vendaAux.Id = 1;
                vendedorAux.Id = 1;
            }

            vendedorAux.CPF = pEntrada.Vendedor.CPF;
            vendedorAux.Nome = pEntrada.Vendedor.Nome;
            vendedorAux.Email = pEntrada.Vendedor.Email;
            vendedorAux.Telefone = pEntrada.Vendedor.Telefone;

            if (pEntrada.Itens.Any())
            {
                foreach (InItem item in pEntrada.Itens)
                {
                    Item itemAux = new Item();
                    itemAux.Identificador = item.Identificador;
                    itemAux.Descricao = item.Descricao;
                    itemAux.Valor = item.Valor;
                    itensAux.Add(itemAux);
                }
            }

            vendaAux.DataVenda = DateTime.Now;
            vendaAux.StatusVenda = StatusVenda.AguardandoPagamento;
            vendaAux.Vendedor = vendedorAux;
            vendaAux.Itens = itensAux;
            vendasAux.Add(vendaAux);

            return vendasAux;
        }

        public bool CriticarRegistro(InRegistrarVenda pEntrada)
        {
            if (!pEntrada.Itens.Any())
                return false;

            if (string.IsNullOrEmpty(pEntrada.Vendedor.CPF))
                return false;

            return true;
        }

        public Vendas RegistrarVenda(InRegistrarVenda pEntrada, Vendas pVendas)
        {
            Venda vendaAux = new Venda();
            Vendedor vendedorAux = new Vendedor();
            List<Item> itensAux = new List<Item>();
            Vendas vendasAux = pVendas;

            if (vendasAux.Any())
            {
                vendaAux.Id = vendasAux.LastOrDefault().Id + 1;
                vendedorAux.Id = vendasAux.LastOrDefault().Vendedor.Id + 1;
            }
            else
            {
                vendaAux.Id = 1;
                vendedorAux.Id = 1;
            }

            vendedorAux.CPF = pEntrada.Vendedor.CPF;
            vendedorAux.Nome = pEntrada.Vendedor.Nome;
            vendedorAux.Email = pEntrada.Vendedor.Email;
            vendedorAux.Telefone = pEntrada.Vendedor.Telefone;

            if (pEntrada.Itens.Any())
            {
                foreach (InItem item in pEntrada.Itens)
                {
                    Item itemAux = new Item();
                    itemAux.Identificador = item.Identificador;
                    itemAux.Descricao = item.Descricao;
                    itemAux.Valor = item.Valor;
                    itensAux.Add(itemAux);
                }
            }

            vendaAux.DataVenda = DateTime.Now;
            vendaAux.StatusVenda = StatusVenda.AguardandoPagamento;
            vendaAux.Vendedor = vendedorAux;
            vendaAux.Itens = itensAux;
            vendasAux.Add(vendaAux);

            return vendasAux;
        }

        /// <summary>
        /// Atualiza status da venda 
        /// </summary>
        /// <param name="pStatus">Novo status</param>
        /// <param name="pVenda">venda a ser modificada</param>
        /// <returns></returns>
        public bool ValidaTrocaStatus(StatusVenda pStatus, Venda pVenda)
        {
            bool statusValido = false;

            switch (pStatus)
            {
                case StatusVenda.PagamentoAprovado:
                    if (pVenda.StatusVenda == StatusVenda.AguardandoPagamento)
                    {
                        statusValido = true;
                    }
                    break;
                case StatusVenda.Cancelada:
                    if (pVenda.StatusVenda == StatusVenda.AguardandoPagamento
                        || pVenda.StatusVenda == StatusVenda.PagamentoAprovado)
                    {
                        statusValido = true;
                    }
                    break;
                case StatusVenda.EnviadoTranportadora:
                    if (pVenda.StatusVenda == StatusVenda.PagamentoAprovado)
                    {
                        statusValido = true;
                    }
                    break;
                case StatusVenda.Entregue:
                    if (pVenda.StatusVenda == StatusVenda.EnviadoTranportadora)
                    {
                        statusValido = true;
                    }
                    break;
            }

            return statusValido;
        }
        
    }
}
